import React from "react";
import Slider from "../layouts/Slider";
import Footer from "../layouts/Footer";
import Library from "../layouts/Library";
import Header from "../layouts/Header";
const Home = () => {
  return (
    <>
      <section className="header__section">
        <Header />
      </section>
      <section className="slider__section">
        <Slider />
      </section>
      <section className="library__section">
        <Library />
      </section>
      <section className="footer__section">
        <Footer />
      </section>
    </>
  );
};

export default Home;
