import React from "react";
import Icons from "../components/Icons";
import Form from "../components/Form";

const Footer = () => {
  return (
    <section className="footer__container">
      <Form />
      <div className="footer__social">
        <div className="footer__socialIcon">
          <Icons type="facebook" />
        </div>

        <div className="footer__socialIcon">
          <Icons type="twitter" />
        </div>

        <div className="footer__socialIcon">
          <Icons type="googleplus" />
        </div>

        <div className="footer__socialIcon">
          <Icons type="pinterest" />
        </div>

        <div className="footer__socialIcon">
          <Icons type="facebook" />
        </div>
      </div>
      <div className="footer__contact">
        <div className="footer__contact-details">
          Alameda Santos, 1970 6th floor - Jardim Paulista São Paulo - SP +55 11
          3090 8500
        </div>
        <div className="footer__contact-details">
          London - UK 125 Kingsway London WC2B 6NH{" "}
        </div>
        <div className="footer__contact-details">
          Lisbon - Portugal Rua Rodrigues Faria, 103 4th floor Lisbon - Portugal
        </div>
        <div className="footer__contact-details">
          Curitiba – PR R. Francisco Rocha, 198 Batel – Curitiba – PR
        </div>
        <div className="footer__contact-details">
          Buenos Aires – Argentina Esmeralda 950 Buenos Aires B C1007
        </div>
      </div>
    </section>
  );
};

export default Footer;
