import React from "react";
import Icons from "../components/Icons";
import Header from "./Header";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";

const Slider = () => {
  return (
    <Slide arrows={false} indicators={true}>
      <div className="each-slide">
        <section className="slider__container">
          <div className="slider__description">
            <h1 className="slider__title">Pixter Digital Books</h1>
            <h4 className="slider__subtitle">
              Lorem ipsum dolor sit amet? consectetur elit, volutpat.
            </h4>
            <p className="slider__description">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              vitae eros eget tellus tristique bibendum. Donec rutrum sed sem
              quis venenatis. Proin viverra risus a eros volutpat tempor. In
              quis arcu et eros porta lobortis sit
            </p>
            <div className="slider__store">
              <div className="slider__storeIcon">
                <Icons type="apple" />
              </div>
              <div className="slider__storeIcon">
                <Icons type="android" />
              </div>
              <div className="slider__storeIcon">
                <Icons type="windows" />
              </div>
            </div>
          </div>
          <Icons type="preview" />
        </section>
      </div>
      <div className="each-slide">
        <section className="slider__container">
          <div className="slider__description">
            <h1 className="slider__title">Pixter Digital Books</h1>
            <h4 className="slider__subtitle">
              Lorem ipsum dolor sit amet? consectetur elit, volutpat.
            </h4>
            <p className="slider__description">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              vitae eros eget tellus tristique bibendum. Donec rutrum sed sem
              quis venenatis. Proin viverra risus a eros volutpat tempor. In
              quis arcu et eros porta lobortis sit
            </p>
            <div className="slider__store">
              <div className="slider__storeIcon">
                <Icons type="apple" />
              </div>
              <div className="slider__storeIcon">
                <Icons type="android" />
              </div>
              <div className="slider__storeIcon">
                <Icons type="windows" />
              </div>
            </div>
          </div>
          <Icons type="preview" />
        </section>
      </div>
      <div className="each-slide">
        <section className="slider__container">
          <div className="slider__description">
            <h1 className="slider__title">Pixter Digital Books</h1>
            <h4 className="slider__subtitle">
              Lorem ipsum dolor sit amet? consectetur elit, volutpat.
            </h4>
            <p className="slider__description">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              vitae eros eget tellus tristique bibendum. Donec rutrum sed sem
              quis venenatis. Proin viverra risus a eros volutpat tempor. In
              quis arcu et eros porta lobortis sit
            </p>
            <div className="slider__store">
              <div className="slider__storeIcon">
                <Icons type="apple" />
              </div>
              <div className="slider__storeIcon">
                <Icons type="android" />
              </div>
              <div className="slider__storeIcon">
                <Icons type="windows" />
              </div>
            </div>
          </div>
          <Icons type="preview" />
        </section>
      </div>
    </Slide>
  );
};

export default Slider;
