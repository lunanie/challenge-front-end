import React from "react";
import Icons from "../components/Icons";

const Header = () => {
  return (
    <nav className="header__nav">
      <Icons type="pixter" />
      <div className="header__links">
        <h3 className="header__text">Books</h3>
        <h3 className="header__text">Newsletter</h3>
        <h3 className="header__text">Address</h3>
      </div>
    </nav>
  );
};

export default Header;
