import React, { useState, useEffect } from "react";
import Books from "../components/Books";
import booksListApi from "../services/api";

const Library = () => {
  const [page, setPage] = useState(0);
  const [booksResp, setBooksResp] = useState([]);
  useEffect(() => {
    const paginateBooks = async () => {
      setBooksResp(await booksListApi());
    };
    paginateBooks();
  }, [page]);
  return (
    <section className="library__container">
      <h2 className="library__title">Books</h2>
      <p className="library__subtitle">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae
        eros eget tellus tristique bibendum. Donec rutrum sed sem quis
        venenatis.
      </p>
      <div className="library__deck">
        {booksResp.items &&
          booksResp.items.map((item, index) => (
            <div key={index} className="library__bookItem">
              <Books
                key={index}
                alt={item.volumeInfo.title}
                title={item.volumeInfo.title}
                bookTitle={item.volumeInfo.title}
                bookSubTitle={item.volumeInfo.subtitle}
                bookDescription={item.volumeInfo.description}
                publishedDate={item.volumeInfo.publishedDate}
                bookCover={
                  item.volumeInfo.imageLinks
                    ? item.volumeInfo.imageLinks.thumbnail
                    : "https://semantic-ui.com/images/wireframe/square-image.png"
                }
              />
            </div>
          ))}
      </div>
    </section>
  );
};

export default Library;
