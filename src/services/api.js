import axios from "axios";

const booksListApi = () => {
  return new Promise((resolve, reject) => {
    axios
      .get("https://www.googleapis.com/books/v1/volumes?q=HARRY%20POTTER")
      .then((resp) => {
        resolve(resp.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default booksListApi;
