import React from "react";
import Modal from "react-modal";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    maxWidth: 600,
  },
};

const Books = ({
  bookCover,
  alt,
  title,
  bookTitle,
  bookSubTitle,
  bookDescription,
  publishedDate,
}) => {
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };
  return (
    <div className="library__book">
      <img
        onClick={openModal}
        className="books__image"
        title={title}
        src={bookCover}
        alt={alt}
      ></img>
      <div>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <div className="modal__title">{bookTitle}</div>
          <div className="modal__date">Data de publicação: {publishedDate}</div>
          <div className="modal__subtitle">{bookSubTitle}</div>
          <div className="modal__description">{bookDescription}</div>
          <button className="modal__button" onClick={closeModal}>
            Fechar
          </button>
        </Modal>
      </div>
    </div>
  );
};

export default Books;
