import React from "react";
import facebook from "../assets/icons/facebook.svg";
import googleplus from "../assets/icons/googleplus.svg";
import twitter from "../assets/icons/twitter.svg";
import android from "../assets/icons/android.svg";
import apple from "../assets/icons/apple.svg";
import windows from "../assets/icons/windows.svg";
import pinterest from "../assets/icons/pinterest.svg";
import pixter from "../assets/icons/pixter_logo.svg";
import preview from "../assets/icons/preview.svg";

const Icons = ({ type }) => {
  const selectIcon = (name) => {
    const icon = {
      facebook,
      googleplus,
      twitter,
      android,
      apple,
      windows,
      pinterest,
      pixter,
      preview
    };
    return icon[name];
  };
  return (
    <img
      className={`icon__${type}`}
      src={selectIcon(type)}
      alt={`Icon ${type}`}
    />
  );
};

export default Icons;
