import React, { useState } from "react";

const Form = () => {
  const [email, setEmail] = useState("Enter your e-mail to receive news");
  const payload = {
    siteName:"Pixter Books",
    email: email,
  };
  console.log(payload);
  return (
    <div className="form__container">
      <h3 className="form__title">Keep in touch with us</h3>
      <p className="form__description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae
        eros eget tellus tristique bibendum. Donec rutrum sed sem quis
        venenatis.
      </p>
      <div className="form__inputContainer">
        <input
          type="text"
          className="form__input"
          onFocus={(e) => setEmail("")}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        ></input>
      </div>
    </div>
  );
};

export default Form;
